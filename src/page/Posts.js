import React from "react";
import {PostsView} from "../views/PostsView";

export class Posts extends React.Component {

    constructor(props) {
        super(props);
        let posts = (this.props.posts != null)? this.props.posts : null;
        this.state = {posts: posts};
    }

    render() {
        return (
            <div id="posts-page">
                <PostsView posts={this.props.posts}/>
            </div>
        );
    }
}