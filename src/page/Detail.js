import React from "react";
import OwlCarousel from "react-owl-carousel2";
import {NavLink} from "react-router-dom";
import {QRCode} from 'react-qr-svg';

export class Detail extends React.Component {

    constructor(props) {
        super(props);
        let id = (this.props.match.params.id != null)? this.props.match.params.id : 0;
        let posts = (this.props.posts != null)? this.props.posts : null;
        let post = this.getPostFromPosts(posts, id);
        this.state = {id: id, post: post, posts: posts};
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        console.log("detail");
        console.log(nextProps);
        console.log(this.props);
        console.log(nextState);
        console.log(this.state);
        if (nextProps.posts != null && this.props.posts == null) {
            return true;
        }
        if (nextProps.posts != null && nextProps.posts.hasOwnProperty(this.props.id) && this.props.posts != null && !this.props.posts.hasOwnProperty(this.props.id)) {
            return true;
        }
        if (nextState.post != null && this.state.post == null) {
            return true;
        }
        if (this.props.match.params.id !== nextState.id) {
            return true;
        }
        if (nextState.post == null && nextProps.posts != null) {
            return true;
        }
        return false;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log("update detail");
        let posts = this.props.posts;
        let post = this.getPostFromPosts(this.props.posts, this.state.id);
        this.setState({id: this.state.id, post: post, posts: posts});
    }

    getPostFromPosts(posts, id) {
        if (posts != null && posts.hasOwnProperty(id)) {
            return posts[id];
        }
        return null;
    }

    render() {
        console.log("render single");
        console.log(this.state);
        let images = [];
        let media = (this.state.post != null)? this.state.post.media : [];
        let user = (this.state.post != null)? this.state.post.user : {};
        let post = (this.state.post != null)? this.state.post : {};

        if (media != null) {
            media[999] = {
                type: 'qr',
                content: post.url
            };
        }

        for (let i in media) {
            if (media.hasOwnProperty(i)) {
                let item = media[i];
                let tag = null;
                switch (item.type) {
                    case "image":
                        tag = (<img src={item.content} alt={`${post.id} #${i}`}/>);
                        break;
                    case "video":
                        tag = (<video controls>
                            <source src={item.content} type="video/mp4"/>
                        </video>);
                        break;
                    case "qr":
                        tag = (
                            <div>
                                <QRCode value={item.content} bgColor="#E8E8E8" fgColor="#000000"/>
                                <span>Scan me!</span>
                            </div>
                        );
                        break;
                    default:
                        tag = (<p>Error while loading media...</p>);
                        break;
                }
                images.push(
                    <div key={i} className={`media-item ${item.type}`}>
                        {tag}
                    </div>
                );
            }
        }

        const options = {
            items: 1,
            nav: false,
            autoplay: false
        };

        const events = {};

        return (
            <div id="single-post">
                <div className="overlay">
                    <div className="container">
                        <div className="post">
                            <div className="back">
                                <NavLink to={"/"}><i className="far fa-arrow-left"/></NavLink>
                            </div>
                            <div className="media">
                                <OwlCarousel ref="media" options={options} events={events}>
                                    {images}
                                </OwlCarousel>
                            </div>
                            <div className="below">
                                <div className="user">
                                    <a href={user.url} target="_blank" rel="noopener noreferrer">
                                        <div className="badge">
                                            <img src={user.image} alt={user.username}/>
                                            <p>{user.username}</p>
                                        </div>
                                    </a>
                                </div>
                                <div className="content">
                                    <div className="description" dangerouslySetInnerHTML={{__html: post.description}}/>
                                </div>
                                <div className="items">
                                    <div className="item">
                                        <i className={`fab fa-${post.platform}`}/>
                                        <p>{post.platform}</p>
                                    </div>
                                    <div className="item">
                                        <i className="far fa-calendar-alt"/>
                                        <p>{post.date}</p>
                                    </div>
                                    <div className="item">
                                        <i className="far fa-heart"/>
                                        <p>{post.likes}</p>
                                    </div>
                                    <div className="item">
                                        <i className="far fa-comment"/>
                                        <p>{post.comments}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}