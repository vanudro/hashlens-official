import React from "react";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import {Posts} from "./page/Posts";
import {Detail} from "./page/Detail";
import {Loader} from "./components/Loader";
import $ from "jquery";

export class App extends React.Component {

    static getApiUrl(suffix) {
        return `http://localhost:8080/${suffix}`;
    }

    constructor(props) {
        super(props);
        this.errorLoadingPosts = this.errorLoadingPosts.bind(this);
        this.setLoaderState = this.setLoaderState.bind(this);

        this.state = {posts: null, loader: this.getLoaderState("Starting Hashlens", false)};
    }

    componentDidMount() {
        let that = this;
        // let url = "https://demo.robinvanuden.com/hashlens/";
        let url = App.getApiUrl("posts/from/instagram");
        console.log(`Make API call to ${url}`);
        let posts = localStorage.getItem("posts");
        if (posts != null){
            that.setToState("posts", JSON.parse(posts));
            console.log(posts);
        }
        console.log(this.state);
        $.ajax({
            url: url,
            method: "GET",
            contentType: "application/json",
            success: function (response) {
                console.log("Getting data");
                that.setLoaderState("Loading posts", true);
                if (response.data != null) {
                    console.log(JSON.stringify(response.data).length);
                    window.localStorage.setItem("posts", JSON.stringify(response.data));
                    that.setToState("posts", response.data);
                } else {
                    that.setLoaderState("Unable to load posts...", false);
                }
            },
            error: function () {
                if (posts == null){
                    that.setLoaderState("Unable to connect to Hashlens...", false);
                } else {
                    that.setLoaderState("Local storage", true);
                    that.setToState("posts", JSON.parse(posts));
                    console.log(posts);
                }
            }
        })
    }

    // shouldComponentUpdate(nextProps, nextState, nextContext) {
    //     if (this.props.posts == null && nextProps.posts != null) {
    //         return true;
    //     }
    //     if (this.state.loader.text !== nextState.loader.text) {
    //         return true;
    //     }
    //     if (this.state.loader.hidden !== nextState.loader.hidden) {
    //         return true;
    //     }
    //     if (this.state.loader.class !== nextState.loader.class) {
    //         return true;
    //     }
    //     return false;
    // }
    //
    // componentDidUpdate() {
    //     console.log("update");
    //     if (this.state.posts != null) {
    //         this.setLoaderState("Finishing up", true);
    //     }
    //     this.setLoaderState(this.state.loader.text, this.state.loader.hidden, this.state.loader.class);
    // }

    setToState(name, value) {
        let state = this.state;
        state[name] = value;
        this.setState(state);
    }

    getLoaderState(text, hidden, _class) {
        return {text: text, hidden: hidden, class: _class};
    }

    setLoaderState(text, hidden, _class) {
        this.setToState("loader", this.getLoaderState(text, hidden, _class));
    }

    errorLoadingPosts() {
        this.setLoaderState(`Unable to load posts`, false, "text-error");
    }

    render() {
        return (
            <div>
                <Router>
                    <Switch>
                        <Route path="/post/:id" render={({match}) => <Detail match={match} posts={this.state.posts}/>}/>
                        <Route path="/" render={({match}) => <Posts match={match} posts={this.state.posts}/>}/>
                    </Switch>
                </Router>
                <Loader text={this.state.loader.text} hidden={this.state.loader.hidden} class={this.state.loader.class}/>
            </div>
        );
    }
}
