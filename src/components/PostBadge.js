import React from "react";
import {NavLink} from "react-router-dom";

export class PostBadge extends React.Component {

    constructor(props) {
        super(props);
        let post = (this.props.post != null)? this.props.post : {};
        let key = (this.props.id != null)? this.props.id : 0;
        let state = {};
        state.key = key;
        state.post = post;
        state.post.image = (post.media[0].content != null)? post.media[0].content : "";
        this.state = state;
    }

    render() {
        return (
            <div className="post">
                <NavLink to={`/post/${this.state.key}`} className="post-link">
                    <div className="frame">
                        <div className="media" style={{backgroundImage: `url('${this.state.post.image}')`}}/>
                        <div className="overlay">
                            <div className="content">
                                <div className="platform">
                                    <i className={`icon fab fa-${this.state.post.platform}`}/>
                                </div>
                                <div className="user">
                                    <img src={this.state.post.user.image} alt={this.state.post.user.username} className="icon"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </NavLink>
            </div>
        )
    }
}