import React from "react";

export class Loader extends React.Component {

    constructor(props) {
        super(props);
        this.state = this.getStateFromProps(this.props);
    }

    getStateFromProps(props) {
        let text = (props.text != null)? props.text : '';
        let hidden = (props.hidden != null)? props.hidden : true;
        let _class = (props.class != null)? props.class : "";
        return {text: text, hidden: hidden, class: _class};
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (this.state.text !== nextProps.text && nextProps.text != null) {
            return true;
        }
        if (this.state.hidden !== nextProps.hidden && nextProps.hidden != null) {
            return true;
        }
        if (this.state.class !== nextProps.class && nextProps.class != null) {
            return true;
        }
        return false;
    }

    componentDidUpdate() {
        this.setState(this.getStateFromProps(this.props));
    }

    render() {
        return (
            <div id={"loader"} className={`loader ${this.state.hidden? "hidden" : "show"}`}>
                <span className={`special ${this.state.class}`}>{this.state.text}</span>
            </div>
        );
    }
}