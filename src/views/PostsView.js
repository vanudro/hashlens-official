import React from "react";
import {PostBadge} from "../components/PostBadge";

export class PostsView extends React.Component {

    constructor(props) {
        super(props);
        this.state = this.getStateFromProps(this.props);
    }

    getStateFromProps(props) {
        let posts = (props.posts != null)? props.posts : null;
        return {posts: posts};
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log("PostView");
        console.log(nextProps);
        console.log(this.props);
        console.log(nextState);
        console.log(this.state);
        if (nextProps.posts != null && this.props.posts != null && this.state.posts == null) {
            console.log(1);
            return true;
        }
        if (this.props.posts != null && this.state.posts == null) {
            console.log(2);
            return true;
        }
        if (nextProps.posts != null && this.props.posts == null) {
            console.log(2);
            return true;
        }
        return false;
    }

    componentDidUpdate() {
        console.log("update");
        this.setState(this.getStateFromProps(this.props));
    }

    render() {
        let posts = this.state.posts;
        let items = [];

        for (let i in posts) {
            if (posts.hasOwnProperty(i)) {
                let post = posts[i];
                items.push(<PostBadge key={i} id={i} post={post}/>)
            }
        }

        return (
            <div className="posts-view">
                <div className="title text-center">
                    <h1>Hashlens</h1>
                </div>
                <div className="container">
                    <div className="posts">
                        {items}
                    </div>
                </div>
            </div>
        )
    }
}