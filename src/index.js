import React from 'react';
import ReactDOM from 'react-dom';
import './assets/css/master.css';
import {App} from './App';
import * as serviceWorker from './serviceWorker';

console.log("Hashlens run");
console.log("Booting up...");

// if ('serviceWorker' in navigator) {
//     window.addEventListener('load', function() {
//         navigator.serviceWorker.register('service-worker.js').then(function(registration) {
//             // Registration was successful
//             console.log('ServiceWorker registration successful with scope: ', registration.scope);
//         }, function(err) {
//             // registration failed :(
//             console.log('ServiceWorker registration failed: ', err);
//         }).catch(function(err) {
//             console.log(err)
//         });
//     });
// } else {
//     console.log('service worker is not supported');
// }


// Install prompt for PWA
// let deferredPrompt;

window.addEventListener('beforeinstallprompt', function (e) {
    console.log('beforeinstallprompt Event fired');
    e.preventDefault();

    // Stash the event so it can be triggered later.
    // deferredPrompt = e.originalEvent;

    return false;
});

ReactDOM.render(<App/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
